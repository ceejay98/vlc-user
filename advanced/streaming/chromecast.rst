####################################
Stream Videos From VLC To Chromecast
####################################

.. figure::  /images/advanced/streaming/chromecast/chromecast-logo.png

VLC allows you to stream videos to your Chromecast connected monitor/TV screen. Since VLC is a **cross-platform** 
software, you can use this feature on the VLC app for **desktop** as well as **mobile**.

Before you proceed further please ensure the following:

1. Connect your device and Chromecast to the same **WiFi** network.
2. **Update** your VLC app to the latest version. Grab the latest software from `here <https://www.videolan.org/vlc/>`_.
3. It has been observed that the use of a **VPN** hinders the connection between your device and Chromecast. Please disable any VPN before proceeding further, as it may cause trouble in establishing a proper connection.
4. You're good to go now!


***************************
Guide For Desktop App Users
***************************

=================
Start streaming :
=================

* Select :menuselection:`Playback --> Renderer`. A list of nearby available Chromecast devices will be shown.
* Wait for some time if the list of devices isn't shown instantly.
* Click on the desired device (in this case - Living Room TV).

.. figure::  /images/advanced/streaming/chromecast/renderer.jpg

* Insecure site warning may appear. Click on :guilabel:`View Certificate`.

.. figure::  /images/advanced/streaming/chromecast/view_certificate.png
   :align:   center

* Again, Click :guilabel:`Accept Permanently` to add the certificate to your device. From next time you won't be asked about this.

.. figure::  /images/advanced/streaming/chromecast/accept_certificate.png
   :align:   center

* Performance warning may also pop up. If you don't want VLC to warn you again, Choose :guilabel:`Ok,Don't Warn me again`.
* Now you can play any video on VLC. It will be streamed directly to your Chromecast screen.

================
Stop streaming :
================

* Select :menuselection:`Playback --> Renderer`.
* Select :guilabel:`<Local>`.

.. figure::  /images/advanced/streaming/chromecast/disconnect_desktop.png
   :align:   center

* VLC will be successfully disconnected from your Chromecast screen.

=================
Laggy Video Fix :
=================

It might happen that the video doesn't load properly and may therefore lag. To fix this, these measures may help:

1. Switch your router/hotspot from 2.4 GHz to 5 GHz band.

2. You may want to change the conversion quality to low for better performance. Follow these steps to do so:

* Select :menuselection:`Tools --> Preferences`.

.. figure::  /images/advanced/streaming/chromecast/select_preference.png
   :align:   center

* Change setting mode from :guilabel:`Simple` to :guilabel:`All`.

.. figure::  /images/advanced/streaming/chromecast/preference.png
   :align:   center

* Navigate to :menuselection:`Stream Output --> Sout Stream --> Chromecast`.
* Select :guilabel:`Low(low quality and low bandwidth)` from the dropdown list.
* Click on :guilabel:`Save` button.

.. figure::  /images/advanced/streaming/chromecast/conversion_quality.png
   :align:   center

**************************
Guide For Mobile App Users
**************************

=================
Start streaming :
=================

.. tabs::

   .. tab:: Android

      * Open the VLC app on the mobile phone.
      * Ensure that your mobile and Chromecast devices are on the same WiFi network.
      * You should see the Chromecast icon at the top of the screen. In the figure shown below, the leftmost icon is the Chromecast icon.

      .. figure::  /images/advanced/streaming/chromecast/before_connection.jpg
         :align:   center

      * Tap it and select the desired Chromecast device from the list.

      .. figure::  /images/advanced/streaming/chromecast/display_list.jpg
         :align:   center

      * The Chromecast icon should be filled with white color. It means the connection is successful.

      .. figure::  /images/advanced/streaming/chromecast/after_connection.jpg
         :align:   center

      * Play any video of your choice.

      .. figure::  /images/advanced/streaming/chromecast/play.jpg

   .. tab:: iOS

      * Open the VLC app on the mobile phone.
      * Ensure that your mobile and Chromecast devices are on the same WiFi network.
      * Start playing the video that you want to cast.
      * You should see the Chromecast icon at the top of the screen. In the figure shown below, the rightmost icon (circled) is the Chromecast icon.

      .. figure::  /images/advanced/streaming/chromecast/ios_chromecast.png
         :align:   center

      * Tap it and select the desired Chromecast device from the list.

      .. figure::  /images/advanced/streaming/chromecast/display_list.jpg
         :align:   center

      * The video will start playing on your Chromecast screen.

================
Stop streaming :
================

.. tabs::

   .. tab:: Android

      * Go back to the main VLC screen.
      * Tap the cast icon again.

      .. figure::  /images/advanced/streaming/chromecast/after_connection.jpg
         :align:   center

      * Tap on disconnect. Your device will be successfully disconnected from the Chromecast screen.

      .. figure::  /images/advanced/streaming/chromecast/disconnect_mobile.jpg
         :align:   center

   .. tab:: iOS

      * Tap the cast icon again.

      .. figure::  /images/advanced/streaming/chromecast/ios_chromecast.png
         :align:   center
      
      * Tap on disconnect. Your device will be successfully disconnected from the Chromecast screen.

      .. figure::  /images/advanced/streaming/chromecast/disconnect_mobile.jpg
         :align:   center
